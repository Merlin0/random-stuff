import requests
from fastapi import FastAPI, Request, Response
from fastapi.staticfiles import StaticFiles
from starlette.templating import Jinja2Templates

app = FastAPI()

app.mount("/static", StaticFiles(directory="static"), name="static")

templates = Jinja2Templates(directory="templates")

@app.get("/")
def main(request: Request):
    return templates.TemplateResponse("index.html", {"request": request})


@app.get("/gif")
def get_random_gif(request: Request):
    url = "https://api.giphy.com/v1/gifs/random"

    params = {
        "api_key": "YGrxA0iOSeeQK22Bd016iU7PEIVe4P8K",
    }

    response = requests.get(url, params=params)

    if response.status_code == 200:
        gif_url = response.json()["data"]["images"]["downsized"]["url"]
        return templates.TemplateResponse("gif.html", {"request": request, "gif_url": gif_url})
    else:
        return templates.TemplateResponse("errors.html", {"request": request, "error_message": "Failed to retrieve GIF"})


@app.get("/sticker")
def get_random_sticker(request: Request):
    url = "https://api.giphy.com/v1/stickers/random"

    params = {
        "api_key": "YGrxA0iOSeeQK22Bd016iU7PEIVe4P8K",
    }

    response = requests.get(url, params=params)

    if response.status_code == 200:
        sticker_url = response.json()["data"]["images"]["downsized"]["url"]
        return templates.TemplateResponse("sticker.html", {"request": request, "sticker_url": sticker_url})
    else:
        return templates.TemplateResponse("errors.html", {"request": request, "error_message": "Failed to retrieve sticker"})


@app.get("/joke")
def get_random_joke(request: Request):
    url = "https://jokes-by-api-ninjas.p.rapidapi.com/v1/jokes"

    headers = {
        "X-RapidAPI-Key": "d5b6d0a44fmsh6215f43bda3fb73p165fe3jsn79b9488c2697",
        "X-RapidAPI-Host": "jokes-by-api-ninjas.p.rapidapi.com"
    }

    response = requests.get(url, headers=headers)

    if response.status_code == 200:
        joke = response.json()[0]["joke"]
        return templates.TemplateResponse("joke.html", {"request": request, "joke": joke})
    else:
        return templates.TemplateResponse("errors.html", {"request": request, "error_message": "Failed to retrieve joke"})


@app.get("/meme")
def get_random_meme(request: Request):
    url = "https://meme-api.com/gimme"

    response = requests.get(url) 

    if response.status_code == 200:
        meme_url = response.json()["url"]
        return templates.TemplateResponse("meme.html", {"request": request, "meme_url": meme_url})
    else:
        return templates.TemplateResponse("errors.html", {"request": request, "error_message": "Failed to retrieve meme"})
    

@app.get("/api/gif")
def get_random_gif_api(request: Request, save_file: bool = True):
    url = "https://api.giphy.com/v1/gifs/random"

    params = {
        "api_key": "YGrxA0iOSeeQK22Bd016iU7PEIVe4P8K",
    }

    response = requests.get(url, params=params)

    if response.status_code == 200:
        gif_url = response.json()["data"]["images"]["downsized"]["url"]
        gif_response = requests.get(gif_url)
        return Response(content=gif_response.content, media_type="image/gif")
    else:
        return {'message': 'error'}
    

@app.get("/api/sticker")
def get_random_sticker_api(request: Request, save_file: bool = True):
    url = "https://api.giphy.com/v1/stickers/random"

    params = {
        "api_key": "YGrxA0iOSeeQK22Bd016iU7PEIVe4P8K",
    }

    response = requests.get(url, params=params)

    if response.status_code == 200:
        sticker_url = response.json()["data"]["images"]["downsized"]["url"]
        sticker_response = requests.get(sticker_url)
        return Response(content=sticker_response.content, media_type="image/gif")
    else:
        return {'message': 'error'}
    

@app.get("/api/joke")
def get_random_joke():
    url = "https://jokes-by-api-ninjas.p.rapidapi.com/v1/jokes"

    headers = {
        "X-RapidAPI-Key": "d5b6d0a44fmsh6215f43bda3fb73p165fe3jsn79b9488c2697",
        "X-RapidAPI-Host": "jokes-by-api-ninjas.p.rapidapi.com"
    }

    response = requests.get(url, headers=headers)

    return response.json()[0]["joke"]


@app.get("/api/meme")
def get_ranm_meme(request: Request):
    url = "https://meme-api.com/gimme"

    response = requests.get(url) 

    if response.status_code == 200:
        meme_url = response.json()["url"]
        meme_response = requests.get(meme_url)
        return Response(content=meme_response.content, media_type="image/gif")
    else:
        return {'message': 'error'}
