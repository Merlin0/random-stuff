import unittest
from fastapi.testclient import TestClient
from app import app
from unittest.mock import patch

class TestApp(unittest.TestCase):
    def setUp(self):
        self.client = TestClient(app)

    # MAIN TESTS
    def test_main(self):
        response = self.client.get("/")

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.template.name, "index.html")

    # GIF TESTS
    def test_get_random_gif(self):
        response = self.client.get("/gif")
        
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.template.name, "gif.html")


    def test_get_random_gif_failure(self):
        with patch('app.requests.get') as mock_request:
            mock_request.return_value.status_code = 500

            response = self.client.get("/gif")

            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.template.name, "errors.html")
            self.assertIn(b"Failed to retrieve GIF", response.content)

    # STICKER TESTS
    def test_get_random_sticker(self):
        response = self.client.get("/sticker")

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.template.name, "sticker.html")
 

    def test_get_random_sticker_failure(self):
        with patch('app.requests.get') as mock_request:
            mock_request.return_value.status_code = 500

            response = self.client.get("/sticker")

            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.template.name, "errors.html")
            self.assertIn(b"Failed to retrieve sticker", response.content)

    # JOKE TESTS
    def test_get_random_joke(self):
        response = self.client.get("/joke")

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.template.name, "joke.html")


    def test_get_random_joke_failure(self):
        with patch('app.requests.get') as mock_request:
            mock_request.return_value.status_code = 500

            response = self.client.get("/joke")

            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.template.name, "errors.html")
            self.assertIn(b"Failed to retrieve joke", response.content)

    # MEME TESTS
    def test_get_random_meme(self):
        response = self.client.get("/meme")

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.template.name, "meme.html")


    def test_get_random_meme_failure(self):
        with patch('app.requests.get') as mock_request:
            mock_request.return_value.status_code = 500

            response = self.client.get("/meme")

            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.template.name, "errors.html")
            self.assertIn(b"Failed to retrieve meme", response.content)

    # OTHER TESTS
    def test_non_existing_endpoint(self):
        response = self.client.get("/non_existing_endpoint")

        self.assertEqual(response.status_code, 404)


